//
//  json-reader.h
//  json-reader
//
//  Created by Mikael Eiman on 2012-02-23.
//  Copyright (c) 2012 TLab West Systems AB. All rights reserved.
//


#ifndef json_reader_json_reader_h
#define json_reader_json_reader_h

#include <stdbool.h>
#include <stddef.h>

/*
 Optional memoization of tokens down to a specified level, speeds
 up search at the expense of a bit of heap. The amount of heap used
 depends on the number of tokens memoized.
*/
#ifndef BSP_JSON_MEMOIZE
	#define BSP_JSON_MEMOIZE 0
#endif
#define MEMOIZE_MAX_LEVELS 1


typedef enum {
	json_err_ok = 0,
	json_err_invalid,
	json_err_not_found,
	json_err_nomem,
} json_err_t; 


typedef enum {
	json_type_invalid = 0,
	json_type_object,
	json_type_string,
	json_type_bool,
	json_type_array,
	json_type_null,
	json_type_number,
} json_type_t; 


typedef struct json_token_t json_token_t;

#if BSP_JSON_MEMOIZE
// only used internally
typedef struct json_memo_list_t json_memo_list_t;
#endif

struct json_token_t {
	const char *source;
	
	json_type_t type;
	
	size_t start;
	size_t end;
    
	size_t size; // num_items if array, end-start otherwise
	
    size_t _pos;
#if BSP_JSON_MEMOIZE
	int level;
	json_memo_list_t *lookup;
#endif
};

/*
 Checks if str is valid JSON.
 
 On success, token can be used as a starting point for json_find()
 Pair each -successful- call to json_validate() with a call to json_free()
 */
json_err_t json_validate(const char *str, json_token_t *token);
/*
 Free memory used by json-reader module for token. 
 */
void json_free(json_token_t *token);

/*
 Prepare an array token for iteration with json_array_next()
 */
void json_array_prepare(json_token_t *array);
/*
 Iterate through an array. Returns json_err_not_found when all items have
 been read.
 */
json_err_t json_array_next(json_token_t *array, json_token_t *value);

/*
 Look for a value by path. Supported path elements:
 name/sub-name:index/sub-sub. Must start with a name, not an index.
 */
json_err_t json_find(const json_token_t *object, const char *path, json_token_t *value);

/*
 Extract a value from a token. Only integer numbers supported currently.
 */
// These functions modify value->_pos
json_err_t json_decode_int(json_token_t *value, int *target, int default_value);
json_err_t json_decode_string(json_token_t *value, char *buffer, size_t buffer_size);
json_err_t json_decode_bool(json_token_t *value, bool *target, bool default_value);
/*
 Like strcmp(), basically.
 */
int json_strcmp(json_token_t *value, const char *str);

#endif
