#ifndef layout_h
#define layout_h

#include <stdint.h>
#include <stdbool.h>


enum {
    LAYOUT_SIZE_INFINITE = 100000
};

typedef enum {
    LAYOUT_ALGO_NONE, // childless
    LAYOUT_ALGO_STACK_HORIZONTAL,
    LAYOUT_ALGO_STACK_VERTICAL,
} layout_algorithm_t;


typedef struct layout_rect_t layout_rect_t;
typedef struct layout_size_t layout_size_t;
typedef struct layout_box_t layout_box_t;
typedef struct layout_padding_t layout_padding_t;


struct layout_rect_t {
    int32_t x;
    int32_t y;
    int32_t width;
    int32_t height;
};

struct layout_size_t {
    layout_rect_t min;
    layout_rect_t max;
    
    layout_rect_t calculated;
    
    layout_rect_t actual;
};


struct layout_padding_t {
    int32_t left;
    int32_t top;
    int32_t right;
    int32_t bottom;
};


struct layout_box_t {
    const char* name;
    layout_box_t *parent;
    layout_box_t *sibling; // next sibling, or NULL
    layout_box_t *children; // the first child box, or NULL
    
    layout_algorithm_t algo;
    
    layout_size_t size;
    layout_padding_t padding;
    
    bool allow_overflow; // rendering supports size larger than available size via e.g. scrolling
};


/*
 * Update the actual sizes of the root box and its children
 */
void layout_update(layout_box_t *root, layout_rect_t canvas);

void layout_prepend_child(layout_box_t *box, layout_box_t *child);
void layout_append_child(layout_box_t *box, layout_box_t *child);
void layout_remove_child(layout_box_t *box, layout_box_t *child);

layout_box_t* layout_box_alloc(layout_algorithm_t algo);
void layout_box_free(layout_box_t *box, bool recursive);

#endif
