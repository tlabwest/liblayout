liblayout
=========

This library is used to determine the dimensions of GUI widgets, based on
a set of simple constraints (min/max dimensions). The purpose is to be able
to easily specify GUIs without having to hard-code any sizes, instead focusing
on the hierarchal arrangement of widgets.

It started out based on the following list of requirements, divided into
layouting and rendering concerns.

Requirements layout lib
-----------------------

* Determine sizes of a set of boxes
* Boxes can contain child boxes
* A child box is limited to the size of the content area of its parent box
* Child boxes are treated according to a selected layout algorithm. Each box can have only one algo, but its children can have different algos as needed.
* A box can have padding?
* A box has a minimum size, a maximum size, and a preferred size.
* If the preferred size is larger than the maximum size, the box needs to handle the overflow in a selected way.
    * Overflow method: hide
    * Overflow method: auto-scroll
    * Overflow method: manual scroll
* A box can present a virtual size to its children, e.g. a scrollable list box would have an unlimited size in either the horizontal or * vertical direction
* If the preferred size is smaller then the actual size, the content of the box can be placed according to a selected alignment. Horizontal and vertical alignment are selected separately, and can be one of: start (left/top), center, end (right/bottom).
* A box has padding, and content area. 
    * The padding is subtracted from the total size, yielding the content area. 
    * Child boxes are placed in the content area, not in padding.
* A box can be resized, causing reevaluation of sizes of other boxes as needed (parent, siblings and up?)
* Resizes are batched to minimize recalculations

Requirements rendering lib
--------------------------

* Takes the boxes laid out by the layout ib and generates graphics
* Boxes are rendered parent first, then children
* Boxes can be animated, i.e. require redraws at regular intervals
* Boxes can be opaque, meaning that they overwrite every pixel within their area
* Rendering can be performed on subsets of boxes, i.e. only redraw animated boxes


Current implementation
----------------------

The current code only concerns itself with the layout part. The most obvious missing
functionality is support for scrollable sub-regions; although it can be implemented
by application code it'll be added here eventually.


Sample code
-----------

The requirements turned into a simple API which you can find in layout.h. Example
code is in main.c, which takes a json file path and turns it into HTML.

    ./liblayout-test test2.json > test2.html
    open test2.html


Limits
------

* min_width defaults to 1, and cannot be smaller
* min_height defaults to 1, and cannot be smaller
* max_width defaults to 100000, cannot be smaller than 1
* max_height defaults to 100000, cannot be smaller than 1


Future work
-----------

* Add support for scrollable boxes
* Add support for application-defined part of `layout_box_t`
* Add soft failures; return error instead of using fail()


License
-------

MIT.
