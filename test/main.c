#include "../layout.h"
#include "../json-reader.h"

#include <stdio.h>
#include <stdlib.h>

enum {
    NUM_COLORS = 7,
};

const char * g_colors[NUM_COLORS] = {
    "#000",
    "#F00",
    "#0F0",
    "#00F",
    "#FF0",
    "#0FF",
    "#F0F",
};

void render_divs(layout_box_t *box)
{
    static int bgcol = 0;
    bgcol = (bgcol + 1) % NUM_COLORS;
    
    printf("<div style='left: %dpx; top: %dpx; width: %dpx; height: %dpx; background: %s;'>\n", 
        box->size.actual.x + box->padding.left, box->size.actual.y + box->padding.top, 
        box->size.actual.width - box->padding.left - box->padding.right, 
        box->size.actual.height - box->padding.top - box->padding.bottom, 
        g_colors[bgcol]
    );
    
    printf("%s\n", box->name);
    
    for ( layout_box_t *curr = box->children; curr != NULL; curr = curr->sibling )
    {
        render_divs(curr);
    }
    
    printf("</div>\n");
}



layout_box_t* process_json(json_token_t *token)
{
    layout_box_t *box = layout_box_alloc(LAYOUT_ALGO_NONE);
    
    // First extract size info, layout algorithm, etc
    json_token_t size; 
    
    if ( json_find(token, "size", &size) == json_err_ok )
    {
        json_token_t min_width; json_find(&size, "min_width", &min_width);
        json_token_t max_width; json_find(&size, "max_width", &max_width);
        json_token_t min_height; json_find(&size, "min_height", &min_height);
        json_token_t max_height; json_find(&size, "max_height", &max_height);
        
        int value;
        if ( json_decode_int(&min_width, &value, 0) == json_err_ok )
        {
            box->size.min.width = value;
        }
        if ( json_decode_int(&max_width, &value, 0) == json_err_ok )
        {
            box->size.max.width = value;
        }
        if ( json_decode_int(&min_height, &value, 0) == json_err_ok )
        {
            box->size.min.height = value;
        }
        if ( json_decode_int(&max_height, &value, 0) == json_err_ok )
        {
            box->size.max.height = value;
        }
    }
    
    json_token_t padding;
    
    if ( json_find(token, "padding", &padding) == json_err_ok )
    {
        json_token_t left; json_find(&padding, "left", &left);
        json_token_t top; json_find(&padding, "top", &top);
        json_token_t right; json_find(&padding, "right", &right);
        json_token_t bottom; json_find(&padding, "bottom", &bottom);
        
        json_decode_int(&left, &box->padding.left, 0);
        json_decode_int(&top, &box->padding.top, 0);
        json_decode_int(&right, &box->padding.right, 0);
        json_decode_int(&bottom, &box->padding.bottom, 0);
    }
    
    json_token_t algo;
    
    if ( json_find(token, "algo", &algo) == json_err_ok )
    {
        if ( json_strcmp(&algo, "stack_horizontal") == 0 )
        {
            box->algo = LAYOUT_ALGO_STACK_HORIZONTAL;
        } else
        if ( json_strcmp(&algo, "stack_vertical") == 0 )
        {
            box->algo = LAYOUT_ALGO_STACK_VERTICAL;
        } else
        {
            printf("Error: unknown layout algorithm\n");
            exit(1);
        }
    }
    
    json_token_t name;
    
    if ( json_find(token, "name", &name) == json_err_ok )
    {
        box->name = calloc(1, 200);
        json_decode_string(&name, (char*)box->name, 200);
    }
    
    // Then get the children
    json_token_t subs;
    
    if ( json_find(token, "subs", &subs) == json_err_ok && subs.type == json_type_array )
    {
        json_token_t sub;
        
        json_array_prepare(&subs);
        
        while ( json_array_next(&subs, &sub) == json_err_ok )
        {
            layout_append_child(box, process_json(&sub));
        }
    }
    
    return box;
}


int main(int numarg, const char **argv)
{
    if ( numarg != 2 )
    {
        printf("Usage: liblayout-test [path to json]\n");
        exit(1);
    }
    
    // read source json
    
    FILE* source = fopen(argv[1], "r");
    
    if ( !source )
    {
        printf("Couldn't open file\n");
        exit(1);
    }
    
    fseek(source, 0, SEEK_END);
    
    size_t filesize = ftell(source);
    
    char *json = calloc(1, filesize+1);
    
    fseek(source, 0, SEEK_SET);
    
    if ( fread(json, filesize, 1, source) != 1 )
    {
        printf("Failed to read file\n");
        exit(1);
    }
    
    json_token_t token;
    
    if ( json_validate(json, &token) != json_err_ok )
    {
        printf("Invalid json\n");
        exit(1);
    }
    
    layout_box_t *root = process_json(&token);
    
    // apply layout
    
    layout_rect_t canvas = {
        0, 0,
        128, 64
    };
    
    printf("<html><head><style>div { position: fixed; } body { margin 10px; }</style></head><body>\n");
    printf("<pre style='float: right;'>\n");
    layout_update(root, canvas);
    printf("</pre>\n");
    
    render_divs(root);
    printf("</body></html>\n");
    
    return 0;
}
