include ../common.mk

TARGET = liblayout-test

#LD = cc
#CC = cc

#CFLAGS = -std=c99 -g -Wall -Wextra
#LDFLAGS = 

LIBRARY=liblayout
LIBNAME=${LIBRARY}.a
HEADERS += $(wildcard *.h)

LIB_SOURCES = $(wildcard src/*.c)
LIB_OBJECTS = $(patsubst %.c,%.c.o,$(LIB_SOURCES))

#TEST_SOURCES = $(wildcard test/*.c)
#TEST_OBJECTS = $(patsubst %.c,%.c.o,$(TEST_SOURCES))

$(TARGET): $(TEST_OBJECTS) $(LIBNAME) Makefile
	@echo "    LD    $@"
	$(LD) $(LDPATH) $(TEST_OBJECTS) $(LIBNAME) $(LFLAGS) -o $(TARGET)

$(LIBNAME): $(LIB_OBJECTS)
	@echo "    AR    $@"
	@$(AR) rcs $(LIBNAME) $(LIB_OBJECTS)

%.c.o: %.c $(HEADERS) Makefile
	@echo "    CC    $@"
	@$(CC) $(CFLAGS) -c $< -o $@

clean:
	@echo "    CLEAN       liblayout"
	@rm -rf $(LIB_OBJECTS) $(TEST_OBJECTS) $(TARGET) $(LIBNAME)
