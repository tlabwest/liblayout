#include "../layout.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Allow application to override LIBLAYOUT_LOG
#ifndef NO_APP_CONFIG
#include "../../app_config.h"
#endif

#if LIBLAYOUT_DEBUG
    #define LIBLAYOUT_LOG(...) { for ( int n=0; n<nesting; ++n ) {printf("  ");}; printf(__VA_ARGS__); }
#else
    #define LIBLAYOUT_LOG(...)
#endif


#ifndef LIBLAYOUT_FAIL
static void __attribute__ ((noreturn))LIBLAYOUT_FAIL(void)
{
    for (;;)
        ;
}
#endif


#ifndef MIN
    #define MIN(a,b) ((a) < (b) ? (a) : (b))
#endif
#ifndef MAX
    #define MAX(a,b) ((a) > (b) ? (a) : (b))
#endif


static layout_rect_t calc_min_size(layout_box_t *box, int nesting)
{
    LIBLAYOUT_LOG("Calc min: %s\n", box->name);
    
    layout_rect_t size;
    
    size.width = box->padding.left + box->padding.right;
    size.height = box->padding.top + box->padding.bottom;
    
    for ( layout_box_t *curr=box->children; curr != NULL; curr = curr->sibling )
    {
        layout_rect_t child_size = calc_min_size(curr, nesting+1);
        
        switch ( box->algo )
        {
            case LAYOUT_ALGO_NONE:
                LIBLAYOUT_FAIL();
                break;
            
            case LAYOUT_ALGO_STACK_HORIZONTAL:
                size.width += child_size.width;
                size.height = MAX(size.height, child_size.height);
                break;
            
            case LAYOUT_ALGO_STACK_VERTICAL:
                size.width = MAX(size.width, child_size.width);
                size.height += child_size.height;
                break;
        }
    }
    
    box->size.calculated.width = MAX(box->size.min.width, size.width);
    box->size.calculated.height = MAX(box->size.min.height, size.height);
    
    LIBLAYOUT_LOG("Min of %s is %dx%d\n", box->name, box->size.calculated.width, box->size.calculated.height);
    
    if ( box->size.calculated.width < 1 || box->size.calculated.height < 1 )
    {
        LIBLAYOUT_FAIL();
    }
    
    return box->size.calculated;
}


static void distribute_remainder(layout_box_t *box, layout_rect_t remainder, int nesting)
{
    LIBLAYOUT_LOG("distribute_remainder: %s {%d, %d}\n", box->name, remainder.width, remainder.height);
    // determine total size of non-maxed children
    layout_rect_t non_maxed = {
        0,0,0,0
    };
    
    for ( layout_box_t *curr=box->children; curr != NULL; curr = curr->sibling )
    {
        if ( curr->size.calculated.width < curr->size.max.width )
        {
            non_maxed.width += curr->size.calculated.width;
        }
        
        if ( curr->size.calculated.height < curr->size.max.height )
        {
            non_maxed.height += curr->size.calculated.height;
        }
    }
    
    // then distribute the remainder proprtionally
    layout_rect_t remains = {
        0,0,0,0
    };
    for ( layout_box_t *curr=box->children; curr != NULL; curr = curr->sibling )
    {
        switch ( box->algo )
        {
            case LAYOUT_ALGO_NONE:
                LIBLAYOUT_FAIL();
                break;
            
            case LAYOUT_ALGO_STACK_HORIZONTAL:
                LIBLAYOUT_LOG("%s height is MIN(%d, %d)\n", 
                    curr->name, 
                    box->size.actual.height, 
                    curr->size.max.height
                );
                curr->size.calculated.height = MIN(box->size.actual.height, curr->size.max.height);
                if ( curr->size.calculated.width < curr->size.max.width )
                {
                    int32_t this_part = (remainder.width*curr->size.calculated.width)/non_maxed.width;
                    curr->size.calculated.width += this_part;
                    LIBLAYOUT_LOG("  %s widening by %d to %d\n", curr->name, this_part, curr->size.calculated.width);
                }
                
                if ( curr->size.calculated.width > curr->size.max.width )
                {
                    int32_t overflow = curr->size.calculated.width - curr->size.max.width;
                    remains.width += overflow;
                    remains.height = remainder.height;
                    LIBLAYOUT_LOG("  %s grew too wide by %d\n", curr->name, overflow);
                    
                    curr->size.calculated.width = curr->size.max.width;
                }
                break;
            
            case LAYOUT_ALGO_STACK_VERTICAL:
                LIBLAYOUT_LOG("%s width is MIN(%d, %d)\n", 
                    curr->name, 
                    box->size.actual.width, 
                    curr->size.max.width
                );
                curr->size.calculated.width = MIN(box->size.actual.width, curr->size.max.width);
                if ( curr->size.calculated.height < curr->size.max.height )
                {
                    int32_t this_part = (remainder.height*curr->size.calculated.height)/non_maxed.height;
                    curr->size.calculated.height += this_part;
                    LIBLAYOUT_LOG("  %s heightening by %d to %d\n", curr->name, this_part, curr->size.calculated.height);
                }
                
                if ( curr->size.calculated.height > curr->size.max.height )
                {
                    int32_t overflow = curr->size.calculated.height - curr->size.max.height;
                    remains.width = remainder.width;
                    remains.height += overflow;
                    LIBLAYOUT_LOG("  %s grew too high by %d\n", curr->name, overflow);
                    
                    curr->size.calculated.height = curr->size.max.height;
                }
                break;
        }
    }
    
    if ( remains.width > 0 || remains.height > 0 )
    {
        distribute_remainder(box, remains, nesting+1);
    }
}


static void apply_size(layout_box_t *box, layout_rect_t within, int nesting)
{
    LIBLAYOUT_LOG("Apply size: %s %dx%d\n", box->name, within.width, within.height);
    
    box->size.actual = within;
    
    // divide available space among children
    // TODO calculate minimum required size to determine what the remainder is
    layout_rect_t min = calc_min_size(box, nesting+1);
    
    layout_rect_t remainder = {
        0, 0,
        box->size.actual.width - min.width - box->padding.left - box->padding.right,
        box->size.actual.height - min.height - box->padding.top - box->padding.bottom
    };
    layout_rect_t prev_remainder = {0,0,0,0};
    
    // apply padding here so that the actual area is what's inside the padding
    box->size.actual.x += box->padding.left;
    box->size.actual.y += box->padding.top;
    //box->size.actual.width -= box->padding.left + box->padding.right;
    //box->size.actual.height -= box->padding.top + box->padding.bottom;
    
    if ( box->children == NULL )
    {
        return;
    }
    
    while ( remainder.width > 0 || remainder.height > 0 )
    {
        prev_remainder = remainder;
        distribute_remainder(box, remainder, nesting+1);
        
        // TODO check if children actually used all the available space?
        // If they didn't the sum of child width/height will differ from box->size.actual
        remainder = box->size.actual;
        
        for ( layout_box_t *curr=box->children; curr != NULL; curr = curr->sibling )
        {
            switch ( box->algo )
            {
                case LAYOUT_ALGO_NONE:
                    LIBLAYOUT_FAIL();
                    break;
                
                case LAYOUT_ALGO_STACK_HORIZONTAL:
                    remainder.width -= curr->size.calculated.width;
                    remainder.height = 0;
                    break;
                
                case LAYOUT_ALGO_STACK_VERTICAL:
                    remainder.width = 0;
                    remainder.height -= curr->size.calculated.height;
                    break;
            }
        }
        
        LIBLAYOUT_LOG("After distributing, remainder is %dx%d\n", remainder.width, remainder.height);
        
        if ( memcmp(&remainder, &prev_remainder, sizeof(remainder)) == 0 )
        { // don't get stuck in a loop
            remainder = (layout_rect_t){0,0,0,0};
        }
    }
    
    for ( layout_box_t *curr=box->children; curr != NULL; curr = curr->sibling )
    {
        apply_size(curr, curr->size.calculated, nesting+1);
    }
}


static void update_positions(layout_box_t *box, layout_rect_t canvas, int nesting)
{
    LIBLAYOUT_LOG("update_positions: %s {%d, %d}\n", box->name, canvas.x, canvas.y);
    
    box->size.actual.x = canvas.x;
    box->size.actual.y = canvas.y;
    box->size.actual.width = canvas.width;
    box->size.actual.height = canvas.height;
    
    layout_rect_t subrect = canvas;
    
    for ( layout_box_t *curr=box->children; curr != NULL; curr = curr->sibling )
    {
        subrect.width = curr->size.actual.width;
        subrect.height = curr->size.actual.height;
        update_positions(curr, subrect, nesting+1);
        
        switch ( box->algo )
        {
            case LAYOUT_ALGO_NONE:
                LIBLAYOUT_FAIL();
                break;
            
            case LAYOUT_ALGO_STACK_HORIZONTAL:
                subrect.x += subrect.width;                
                break;
            
            case LAYOUT_ALGO_STACK_VERTICAL:
                subrect.y += subrect.height;
                break;
        }
    }
}


void layout_update(layout_box_t *box, layout_rect_t canvas)
{
    int nesting = 0;
    
    apply_size(box, canvas, nesting);
    
    update_positions(box, canvas, nesting);
}


layout_box_t* layout_box_alloc(layout_algorithm_t algo)
{
    layout_box_t *res = calloc(1, sizeof(layout_box_t));
    
    if ( res == NULL )
    {
        return NULL;
    }
    
    // some default values
    res->algo = algo;
    res->size.max.width = LAYOUT_SIZE_INFINITE;
    res->size.max.height = LAYOUT_SIZE_INFINITE;
    res->size.min.width = 1;
    res->size.min.height = 1;
    
    return res;
}


void layout_box_free(layout_box_t *box, bool recursive)
{
    if ( box == NULL )
    {
        return;
    }
    
    if ( recursive )
    { // first free all children
        while ( box->children != NULL )
        {
            layout_box_t *next = box->children->sibling;
            layout_box_free(box->children, true);
            box->children = next;
        }
    }
    
    free(box);
}

void layout_prepend_child(layout_box_t *box, layout_box_t *child)
{
    child->parent = box;
    child->sibling = box->children;
    box->children = child;
}


void layout_append_child(layout_box_t *box, layout_box_t *child)
{
    child->parent = box;
    child->sibling = NULL;
    
    if ( box->children == NULL )
    { // first child
        box->children = child;
        return;
    }
    
    // find last child and point it at new child
    layout_box_t *curr = box->children;
    
    while ( curr->sibling != NULL )
    {
        curr = curr->sibling;
    }
    
    curr->sibling = child;
}


void layout_remove_child(layout_box_t *box, layout_box_t *child)
{
    child->parent = NULL;
    
    if ( box->children == NULL )
    {
        return;
    }
    
    if ( box->children == child )
    { // first child
        box->children = box->children->sibling;
        return;
    }
    
    layout_box_t *curr = box->children->sibling;
    
    while ( curr != NULL && curr->sibling != child )
    {
        curr = curr->sibling;
    }
    
    if ( curr != NULL )
    { // found previous sibling
        // re-point to next sibling
        curr->sibling = curr->sibling->sibling;
    }
}


